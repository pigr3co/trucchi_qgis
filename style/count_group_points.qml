<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.4.0-Madeira" styleCategories="Labeling" labelsEnabled="1">
  <labeling type="rule-based">
    <rules key="{8fa7f792-5237-4559-9478-200c0ea8e35d}">
      <rule key="{d52cf026-d425-4f42-b1b2-250fc391ffc6}" filter="is_selected()">
        <settings>
          <text-style fontCapitals="0" previewBkgrdColor="#ffffff" fontWordSpacing="0" fontStrikeout="0" useSubstitutions="0" fontItalic="0" fieldName=" count($id, group_by:=is_selected())" fontUnderline="0" multilineHeight="1" isExpression="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontLetterSpacing="0" fontSizeUnit="Point" fontSize="18" textOpacity="1" fontWeight="50" blendMode="0" fontFamily="MS Shell Dlg 2" namedStyle="Normale" textColor="0,0,0,255">
            <text-buffer bufferSize="1" bufferColor="255,255,255,255" bufferNoFill="1" bufferOpacity="1" bufferDraw="1" bufferBlendMode="0" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferJoinStyle="128" bufferSizeUnits="MM"/>
            <background shapeRotation="0" shapeFillColor="255,255,255,255" shapeBorderColor="128,128,128,255" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeOffsetX="0" shapeBorderWidthUnit="MM" shapeSVGFile="" shapeJoinStyle="64" shapeRadiiY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeSizeUnit="MM" shapeRadiiX="0" shapeOffsetUnit="MM" shapeBlendMode="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1" shapeRotationType="0" shapeSizeX="0" shapeSizeType="0" shapeType="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0" shapeOffsetY="0" shapeDraw="0" shapeRadiiUnit="MM"/>
            <shadow shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetGlobal="1" shadowOpacity="0.7" shadowRadius="1.5" shadowOffsetDist="1" shadowOffsetUnit="MM" shadowRadiusAlphaOnly="0" shadowUnder="0" shadowOffsetAngle="135" shadowDraw="0" shadowBlendMode="6" shadowScale="100" shadowColor="0,0,0,255" shadowRadiusUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0"/>
            <substitutions/>
          </text-style>
          <text-format autoWrapLength="0" formatNumbers="0" decimals="3" multilineAlign="3" leftDirectionSymbol="&lt;" addDirectionSymbol="0" plussign="0" rightDirectionSymbol=">" reverseDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" wrapChar=""/>
          <placement priority="5" dist="0" offsetType="0" distMapUnitScale="3x:0,0,0,0,0,0" placementFlags="10" centroidInside="0" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" repeatDistance="0" quadOffset="4" maxCurvedCharAngleOut="-25" centroidWhole="0" maxCurvedCharAngleIn="25" placement="0" preserveRotation="1" distUnits="MM" xOffset="0" fitInPolygonOnly="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" yOffset="0" rotationAngle="0" repeatDistanceUnits="MM" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0"/>
          <rendering obstacleType="0" fontMinPixelSize="3" zIndex="0" drawLabels="1" mergeLines="0" displayAll="0" minFeatureSize="0" obstacleFactor="1" fontLimitPixelSize="0" scaleMax="0" maxNumLabels="2000" obstacle="1" limitNumLabels="0" upsidedownLabels="0" fontMaxPixelSize="10000" labelPerPart="0" scaleVisibility="0" scaleMin="0"/>
          <dd_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option type="Map" name="properties">
                <Option type="Map" name="PositionX">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="x(centroid(collect( $geometry, group_by:=is_selected())))" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
                <Option type="Map" name="PositionY">
                  <Option type="bool" value="true" name="active"/>
                  <Option type="QString" value="y(centroid(collect( $geometry, group_by:=is_selected())))" name="expression"/>
                  <Option type="int" value="3" name="type"/>
                </Option>
              </Option>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </dd_properties>
        </settings>
      </rule>
    </rules>
  </labeling>
  <layerGeometryType>0</layerGeometryType>
</qgis>
